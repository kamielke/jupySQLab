How to use the juSQLab class

1. copy jupySQLab.py file to the folder your python can access
2. In jupyter cell type e.g.
import jupySQLab as jsql
x=jsql.juSQLab()
and run the cell

(you can use any name for browser, not only x)

You will see widgets (see screen jupySQLab.png)

3. Enter database name you want to open or create and press Connect button
When the connection is established the button will change to Disconnect button

5. You can
	run query - type query and press Run Query button
	define/change tables - type DDL statement and press Execute DDL
	export quey result to excel file - type file name WITH EXTENSION .xlsx and press Export button
	change database - press disconnect button and choose another database
	access connection by x.conn
	access query result by x.result (of dataframe type) till the next query
	define schema based on excel file - schema = x.CreateSchema(<filename with the schema>)
	load data into tables based on list in execl file x.LoadData(<filename with files to load>,schema)
See details of methods in python comments

6. You can open as many browsers as you can with different names e.g. y=juSQLab(),
but they must be run in different cells.

7. The widgets will disapper after safe, therefore if you want to store any result, just 
run x.result in a cell and save the notebook.  

8. If you want to show query results to non-jupyter user, save the page in HTML.
When saved as completed page and uploaded e.g. on sharepoint it is visible in a web browser, 
via link from sharepoint, without possibility to rerun and without query tool.

When to use the tool
- if you know SQL much better than pandas
- if you do not know in advance what joins, queries will be most useful for your data 
AND you do NOT have access to proper database tool with import feature from files. 
- if you want to show results of various queries to non-jupyter user, non-db user