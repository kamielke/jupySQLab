#----------------------------------------
# Author: Kalina Mielke 26.06.2018
#----------------------------------------
# mini SQLite browser object in jupyter Notebook
# supports SQLite database:
#	connection
#	creation
#	query browser 
#	DDL execution
#	query result export to excel file
#	query result in DataFrame
#----------------------------------------
# object creation: juSQLab()
# object variables:
# 	conn - connection to database
# 	result - pd.DataFrame with query result
# methods:
#	CreateSchema(<schema filename>) - reads schema from excell file, creates tables accordingly, 
#                                     returns DataFrame with the schema
# 		Excel schema filename format (header as below): 
	# 	Table - a table name
	# 	ColNo - a column number, one column with unique number 1 is required
	# 	ColName - a column name
	# 	ColType - a column type, according to sqlite syntax, optional
	# 	ColUni - an aditional part of creation statement, optional, may contain full index, constraints definition as well
	# 	Used - is the column used in database True - yes, False - no, used only in a loaded file.
#	LoadData(<files list filename>, <schema DataFrame>) - loads data from files defined in filename, according schema columns 
# 	Excel file list format
	#   Finame - file name with fill path,
	#   Table - table name,
	#   Sep - 'C' in case comma separated file, otherwise null ("\t" is a default separator)
	#   Head - "N" in case of None, otherwise null (0 is default i.e one line of header)
#------------------------------------------------------------
import ipywidgets as widgets
from IPython.display import clear_output
import pandas as pd
import numpy as np
import sqlite3


class juSQLab:
    
    def __init__(self):
        result=None
        conn=None
        qry = widgets.Textarea(
            value='',
            placeholder='Enter query',
            description='Query/DDL:',
            disabled=False,
            layout=widgets.Layout(height='65px',width='80%'))

        rbtn= widgets.Button(
            description='Run SQL query',
            disabled=False)

        ebtn= widgets.Button(
            description='Export result',
            disabled=False)

        sbtn= widgets.Button(
            description='Execute DDL',
            disabled=False)

        dbtn= widgets.Button(
            description='Connect',
            disabled=False)

        dbname=widgets.Text(
            value='',
            placeholder='Enter db name',
            description='Database:',
            disabled=False,
            layout=widgets.Layout(width='80%'))

        exfiname=widgets.Text(
            value='',
            placeholder='Enter file name',
            description='Export to:',
            disabled=False,
            layout=widgets.Layout(width='80%'))
        
        def disp_widgets():
            display(widgets.VBox([widgets.HBox([dbname,dbtn]),
                        widgets.HBox([qry,widgets.VBox([rbtn,sbtn])]),
                        widgets.HBox([exfiname,ebtn])]))

        def run_query(b):
            clear_output(wait=True)
            try:
                self.result=pd.read_sql_query(qry.value,self.conn)
                table_layout = widgets.Layout(height='500px',overflow_y='auto',overflow_x='auto')
                tab=widgets.HTML(value=pd.DataFrame.to_html(self.result),layout=table_layout )
                disp_widgets()
                display(tab)
            except sqlite3.Error as e:
                disp_widgets()
                print(e.message)
            except Exception as e:
                disp_widgets()
                print("Exception: %s" % e)
    
        def execute_ddl(b):
            try:
                db = self.conn.cursor()
                db.execute(qry.value)
                self.conn.commit()
            except sqlite3.Error as e:
                print(e.message)
            except Exception as e:
                print("Exception: %s" % e)
        
        def export_result(b):
            try:
                writer=pd.ExcelWriter(exfiname.value)
                self.result.to_excel(writer,'Sheet1')
                writer.book.add_worksheet('Query').write(0,0,qry.value)
                writer.save()
                print("Result exported.")
            except Exception as e:
                print("Exception: %s" % e)
        
        def connect_db(b):
            if b.description == 'Connect':
                try:
                    self.conn = sqlite3.connect(dbname.value)
                    b.description = 'Disconnect'
                except Exception as e:
                    print("Exception: %s" % e)
            else :
                b.description = 'Connect'
                self.conn.close()
                dbname.value=''
            clear_output(wait=False)
            disp_widgets()
    
        rbtn.on_click(run_query)
        ebtn.on_click(export_result)
        sbtn.on_click(execute_ddl)
        dbtn.on_click(connect_db)
        self.result=pd.DataFrame()
        disp_widgets()
    
    def CreateSchema(self,fischemname) :
    # function for db schema creation, returns dataframe with schema from file fischemname
    # adds new tables to the existing database 
    # there must be valid conn connection to the database for the object
    # fischemname - name of an excel file with schema
    # Excel format : 
	# Table - a table name
	# ColNo - a column number, one column with unique number 1 is required
	# ColName - a column name
	# ColType - a column type, according to sqlite syntax, optional
	# ColUni - an aditional part of creation statement, optional, may contain full index, constraints definition as well
	# Used - is the column used in database True - yes, False - no, used only in a loaded file.
	# The same, but dataFrame structure is used while loading data into datatable. 
	# All the columns should be listed, but only used are loaded.
        def CreateTab (tabnam,schem) : 
		#Create table statement for tabnam
            stmt = "Create table if not exists " + tabnam + " ("
            for index, row in schem[(schem.Table == tabnam) & (schem.Used==True)].iterrows() :
                stmt = stmt + ' '+ row.ColName + " " + row.ColType + ' ' + row.ColUni +","
            return stmt[:-1] + ')'
    
        schem = pd.read_excel(fischemname) #read schema
        schem = schem.fillna(value='') #remove NaN
        db = self.conn.cursor() 
    
        for el in schem[schem.ColNo ==1]['Table'].iteritems():
            db.execute(CreateTab(el[1],schem)) #see example below
        self.conn.commit()
        return schem

    def LoadData(self,filist,schem) :
    # procedure for data loading from an excel file with name filist
    # Loading is in append mode only i.e. data is added to the existing 
    # filist - name of file with files to load
    # schem - data frame with db schema, as exported from CreateSchema
	# Excel file format
	# - Finame - file name with fill path,
	# - Table - table name,
	# - Sep - 'C' in case comma separated file, otherwise null ("\t" is a default separator)
	# - Head - "N" in case of None, otherwise null (0 is default i.e one line of header)
        def TabColumns(tabname,schem,used_only) :
        # makes list of columns for the table , used or all
            tabcol = []
            for index,row in schem[(schem.Table==tabname)&((schem.Used==True)| (used_only==False))].iterrows():
                tabcol.append(row.ColName)
            return tabcol
        fiload = pd.read_excel(filist) #read list of files to load
        for i,row in fiload.iterrows():
            print('Reading ... ' + row.Finame)
            df = pd.read_csv(row.Finame,sep=("," if row.Sep=="C" else "\t"),skipinitialspace=True,\
                 encoding='latin_1',names=TabColumns(row.Table,schem,False), header=(None if row.Head=='N' else 0)\
                         , usecols=TabColumns(row.Table,schem,True))
            print('Loading to table:' + row.Table)
            df.to_sql(row.Table, self.conn, if_exists='append', index=False)
        self.conn.commit()
